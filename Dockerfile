FROM openjdk:11.0.2-jdk-slim-stretch


#================
# Generic stuff
#================
RUN apt-get update -qqy \
  && apt-get -qqy --no-install-recommends install \
    git wget curl gnupg \
  && rm -rf /var/lib/apt/lists/*


#==========
# Maven
#==========
ENV MAVEN_VERSION 3.6.1

RUN curl -fsSL http://archive.apache.org/dist/maven/maven-3/$MAVEN_VERSION/binaries/apache-maven-$MAVEN_VERSION-bin.tar.gz | tar xzf - -C /usr/share \
  && mv /usr/share/apache-maven-$MAVEN_VERSION /usr/share/maven \
  && ln -s /usr/share/maven/bin/mvn /usr/bin/mvn

ENV MAVEN_HOME /usr/share/maven

RUN useradd cyril --shell /bin/bash --create-home
USER cyril
WORKDIR /home/cyril


